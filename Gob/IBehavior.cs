using System;

namespace Gob
{
	public interface IBehavior
	{
		GameTime Time { get; }
	}
}
