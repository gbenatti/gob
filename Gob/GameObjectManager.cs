using System;
using System.Collections.Generic;
using System.Linq;

namespace Gob
{
	public class GameObjectManager
	{
		Dictionary<string, IGameObject> objects = new Dictionary<string, IGameObject>();

		public GameObjectManager ()
		{
		}

		public void CreateObjectWithName (string objectName)
		{
			objects[objectName] = new GameObject (objectName, this);
		}

		public IGameObject Find (string gameObjectName)
		{
			return objects.ContainsKey(gameObjectName) ? objects [gameObjectName] : null;
		}
	}
}

