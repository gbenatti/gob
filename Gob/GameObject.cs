using System;
using System.Collections.Generic;

namespace Gob
{
	public class GameObject : IGameObject
	{
		static GameObjectManager objectManager;

		List<IBehavior> behaviors;

		public static IGameObject Find(string gameObjectName)
		{
			return objectManager.Find (gameObjectName);
		}

		public string Name 
		{
			get;
			private set;
		}

		public GameObject (string name, GameObjectManager manager)
		{
			this.Name = name;
			objectManager = manager;
			behaviors = new List<IBehavior> ();
		}

		void AddComponent<T> (T component) where T : IBehavior
		{
			behaviors.Add (component);
		}

		T GetComponent<T>() where T : IBehavior
		{
			return (IBehavior)behaviors.Find (b => b.GetType() == typeof(T));
		}

		int ComponentCount
		{
			get 
			{
				return behaviors.Count;
			}
		}
	}
}

