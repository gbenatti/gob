using System;
using System.Collections.Generic;

namespace Gob
{
	public interface IGameObject
	{
		void AddComponent<T> (T component) where T : IBehavior;
		T GetComponent<T>() where T : IBehavior;

		int ComponentCount { get; }

		string Name { get; }
	}
}

