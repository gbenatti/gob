using System;
using NUnit.Framework;

namespace Gob.Tests
{
	[TestFixture]
	public class GameObjectCreationTest
	{
		const string testObject 		= "testObject";
		const string anotherTestObject 	= "anotherTestObject";
		const string inexistentObject	= "inexistentObject";

		GameObjectManager manager;

		[SetUp]
		public void Setup()
		{
			manager = new GameObjectManager ();
			manager.CreateObjectWithName (testObject);
			manager.CreateObjectWithName (anotherTestObject);
		}

		[Test]
		public void ShouldBePossibleToFindObjectByName()
		{
			var gameObject = GameObject.Find (testObject);
			Assert.IsNotNull (gameObject);
			Assert.IsTrue(gameObject is IGameObject);
			Assert.AreSame (testObject, gameObject.Name);
		}

		[Test]
		public void ShouldFindMoreThanOneObjectByName()
		{
			var gameObject = GameObject.Find (testObject);

			Assert.IsNotNull (gameObject);
			Assert.IsTrue(gameObject is IGameObject);
			Assert.AreSame (testObject, gameObject.Name);

			gameObject = GameObject.Find (anotherTestObject);

			Assert.IsNotNull (gameObject);
			Assert.IsTrue(gameObject is IGameObject);
			Assert.AreSame (anotherTestObject, gameObject.Name);
		}

		[Test]
		public void CreatingAObjectWithTheSameNameAsAnAlreadyExistentShouldReplaceIt()
		{
			var firstTestObject = GameObject.Find (testObject);

			manager.CreateObjectWithName (testObject);

			var secondTestObject = GameObject.Find (testObject);

			Assert.AreNotSame (firstTestObject, secondTestObject);
		}

		[Test]
		public void ShouldReturnANullIfObjectNotFound()
		{
			var inexistentGameObject = GameObject.Find (inexistentObject);
			Assert.IsNull (inexistentGameObject);
		}
	}
}
