using System;
using NUnit.Framework;

namespace Gob.Tests
{
	[TestFixture]
	public class GameObjectBehaviorsTest
	{
		[Test]
		public void GameObjectShouldBeCreatedWithoutBehaviors()
		{
			var manager = new GameObjectManager ();
			manager.CreateObjectWithName ("teste");

			var obj = manager.Find ("teste");
			Assert.AreEqual (0, obj.Behaviors.Count);
		}
	}
}
